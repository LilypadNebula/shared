### My shared terminal files
These are the terminal files I share between all of my computers. (bash, prompt, etc)

If you are looking for my vim configuration, please see https://gitlab.com/lholden/dot_vim
